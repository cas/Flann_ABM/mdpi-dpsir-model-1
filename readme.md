# Welcome to the full document and code base for the article by Chambers et al., 2024

## Folder **GAMA-model**

This folder contains the model source code (folder **models**, section 4.2) and input data (folder **includes**, section 4.3), as well as the python pipeline (folder **fileWatcher**) for fetching data about travel times from the public transportation API (section 4.4.2) and equipping the agents with this knowledge during the simulation (section 4.4.3).  The folder contains all outputs produced by the simulations, which are then typically moved into the **output-datasets-analysis** folder thanks to the *saveExperiment.py* script.

## Folder **output-datasets-analysis**

This folder contains simulation output datasets. All 6 simulations from section 5 are available. In each folder, the data analysis platform can be launched by executing the associated *app.py* script.

## File **pseudo-code-v2.pptx**

An algorithm written in pseudo-code which details the inner workings of the agent-based model.