Cycles per year: 12
Commuter granularity (persons/agent): 100
Demographic growth (persons): 0
Proportion of commuters changing jobs every year: 0.127
Proportion of commuters favoring low building density over low travel time: 0.0
Normalized low building density value: 0.1
High travel time threshold value (min): 60
Commuter initial patience: between 5 and 7
