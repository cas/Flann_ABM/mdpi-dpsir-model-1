model DPSIRv2

// --- CURRENT BUGS ---

// v2 solves bugs and provides optimizations from v1.
// Old superfluous behaviors have also been removed.
// Most recent version works with file watcher v6. Travel time threshold is no longer hardcoded.
// Instead, each commuter sends its own threshold to python and python takes it into account.
// Varying thresholds are now adequately handled.
// January 10 update: when commuters spontaneously change jobs, they now also recalculate their travel time and
// re-evaluate their happiness. spontaneousJobChange is no longer a reflex but an action, called by the global reflex changeCommutersJobs.

// For some reason, GAMA does not always manage to read the files on time. Currently, this has been circumvented by looping over the opening of the file.
// As a result, keep an eye on any potential infinite loops during runtime.

// --------------------
global {
	date starting_date <- date("2023-01-01");
	float step <- 1 #month update: 1 #month;
	int yearToCycle <- 12;
	int lastCycle <- 36;

	//  ###########################################
	//  ***** Files and world parametrization *****
	//  ###########################################

	// Load shapefiles
	file jobsAddrSHP <- file("../includes/wholeLayers/AddressesEmploisPP1.shp");
	string rentSectorsPath <- '../includes/wholeLayers/wholeCanton_loyers.shp';
	string PDC_path <- "../includes/wholeLayers/PDC_layer.shp";
	string genevaBorderPath <- '../includes/wholeLayers/communesGE.shp';
	string lakePath <- '../includes/wholeLayers/GEO_LAC.shp';
	string tpgTramsPath <- '../includes/wholeLayers/tpgTRAMS.shp';
	string tpgStopsPath <- '../includes/wholeLayers/tpgStops.shp';

	// Filepaths for communicating with python file watcher.
	string tempAddressesPath <- '../fileWatcher/observed/tempAddresses.txt';
	string addressesPath <- '../fileWatcher/observed/vacad.txt';
	string tempRequestsPath <- '../fileWatcher/observed/tempRequests.txt';
	string requestsPath <- '../fileWatcher/observed/requests.txt';
	string responsesPath <- '../fileWatcher/observed/response.csv';

	// Set world boundaries. Warning, this HAS to be done in global BEFORE init, otherwise everything breaks.
	// Which means that the envelope-defining file also needs to be defined in global before init.
	geometry shape <- envelope(jobsAddrSHP);
	list<Addresses> freeHomes;
	int maxJobs;

	//model params
	int commuterGranularity <- 100;
	int demographicGrowth <- 5;
	float jobChangesPerYear <- 0.127;
	float proportionPreferringLowDensity <- 0.1;
	float lowDensityValue <- 0.1;
	int highTThreshValue <- 60;
	float x_percent_free <- 3.0;

	// Prepare deletion of files
	bool deleteTempRequestsOK;
	bool deleteRequestsOK;
	bool deleteTempAddressesOK;
	bool deleteAddressesOK;
	bool deleteResponsesOK;
	bool copyOK;
	bool processOK;
	bool deleteOutputAddressesOK;
	bool deleteOutputCommutersOK;
	string toWrite;
	string toSave;
	int nLines <- 0;

	// Output file paths
	string outputAddressesFolder <- '../output/DPSIR-model/newDataAddresses';
	string outputCommutersFolder <- '../output/DPSIR-model/newDataCommuters';
	string readme <- '../output/DPSIR-model/readme.txt';

	// commuter related variables
	list<Addresses> listVacantHomes;
	Commuters c;

	// log variables
	int nToRelocate <- 0;
	int nRelocated <- 0;

	init {
		do initReadme;
		// Start create agents.
		do initBaseMap;
		create tpgStops from: file(tpgStopsPath) with: [lon::float(read("longitude")), lat::float(read("latitude"))];
		create Government;
		create RentSectors from: file(rentSectorsPath) with: [rent::float(read("LOYER"))];
		do initJobs;
		do initAddresses;
		do initCommuters;
		create PDC_areas from: file(PDC_path) with: [type:: string(read("Type")), lon::float(read("CENTROID_X")), lat::float(read("CENTROID_Y"))];
		// End create agents, begin other init tasks.
		do initPruneExcessCommuters;
		do initFreeHomes;
		do initAssignJobs;
		do initLog;
		do initFiles;
		do initCommuterHappiness;
		// End init.
	}

	action initReadme {
		save 'Cycles per year: ' + yearToCycle to: readme format: txt header: false rewrite: true;
		save 'Commuter granularity (persons/agent): ' + commuterGranularity to: readme format: txt header: false rewrite: false;
		save 'Demographic growth (persons): ' + demographicGrowth to: readme format: txt header: false rewrite: false;
		save 'Proportion of commuters changing jobs every year: ' + jobChangesPerYear to: readme format: txt header: false rewrite: false;
		save 'Proportion of commuters favoring low building density over low travel time: ' + proportionPreferringLowDensity to: readme format: txt header: false rewrite: false;
		save 'Normalized low building density value: ' + lowDensityValue to: readme format: txt header: false rewrite: false;
		save 'High travel time threshold value (min): ' + highTThreshValue to: readme format: txt header: false rewrite: false;
	}

	action initBaseMap {
		create communesBorder from: file(genevaBorderPath) with: [name:: string(read("COMMUNE"))];
		create lake from: file(lakePath);
		create tramLines from: file(tpgTramsPath) with: [lineID::string(read("LIGNE"))];
	}

	action initJobs {
		create Jobs from: jobsAddrSHP with: [nJobs::int(read("EMP2015_CH")) / commuterGranularity, lon::float(read("CENTROID_X")), lat::float(read("CENTROID_Y"))]; // Remove everything from Celigny.
		// Remove from Celigny.
		ask Jobs where (each.location.y < 4000) {
			do die;
		}
		// Remove empty cells.
		ask Jobs where (each.nJobs = 0) {
			do die;
		}

		maxJobs <- Jobs max_of (each.nJobs);
		ask Jobs {
			do colorScale;
		}

	}

	action initAddresses {
		create Addresses from: jobsAddrSHP with:
		[nCommuters::int(read("POP2015_CH")) / commuterGranularity, volumesum::int(read("volumesum")), normDensity::float(read("norm")), lon::float(read("CENTROID_X")), lat::float(read("CENTROID_Y"))]; // Spawn commuters inside Addresses.
		ask Addresses where (each.location.y < 4000) {
			do die;
		}

		ask Addresses where (each.nCommuters = 0) {
			do die;
		}

	}

	action initCommuters {
		ask Addresses {
		// Account for decimals : with a number of commuters of 5.7, 5 commuters are created, with 70% chance of creating a 6th one.
			create Commuters number: int(self.nCommuters) + int(flip(self.nCommuters - int(self.nCommuters))) {
				home <- myself;
				location <- home.location;
				maxRent <- myself.aRent + rnd(0.1, 2.0);
				if flip(proportionPreferringLowDensity) {
					tThreshold <- highTThreshValue;
					normDensityThreshold <- lowDensityValue;
					color <- #pink;
				}

			}

		}

	}

	action initPruneExcessCommuters {
	// In order to ensure 1 job per commuter, we need to remove the surplus commuters
		if length(Commuters) > (Jobs sum_of (each.nJobs)) {
			ask sample(list(Commuters), length(Commuters) - Jobs sum_of (each.nJobs), false) {
				do die;
			}

		}

	}

	action initFreeHomes {
	// Free up x% of the homes, so that agents may move out and move in.
		ask sample(list(Commuters), x_percent_free * length(Commuters) / 100, false) {
			ask home {
				vacancies <- vacancies + 1;
			}

			do die;
		}

	}

	action initAssignJobs {
	// Commuter is assigned a job.
	// Initialize loading counter.
		float n <- 0.0;
		int lastn <- 0;
		ask Commuters {
			cJob <- one_of(Jobs where (each.nJobsLeft > 0));
			ask cJob {
				nJobsLeft <- nJobsLeft - 1;
			}

			do getClosestStopsToJob;

			// Update loading counter.
			n <- n + 100 / length(Commuters);
			if int(n) != lastn {
				write "Assigning jobs to commuters, " + string(lastn + 1) + "% done.";
				lastn <- int(n);
			}

		}

		write "Jobs assigned successfully.";
	}

	action initLog {
		write "Number of commuters: " + length(Commuters);
		write "Number of jobs: " + Jobs sum_of (each.nJobs);
		write "Number of job places: " + length(Jobs);
		write "Number of addresses: " + length(Addresses);
	}

	action initFiles {
		deleteTempAddressesOK <- delete_file(tempAddressesPath);
		deleteAddressesOK <- delete_file(addressesPath);
		deleteTempRequestsOK <- delete_file(tempRequestsPath);
		deleteRequestsOK <- delete_file(requestsPath);
		deleteResponsesOK <- delete_file(responsesPath);
		deleteOutputAddressesOK <- delete_file(outputAddressesFolder);
		deleteOutputCommutersOK <- delete_file(outputCommutersFolder);
	}

	action initCommuterHappiness {
		write "Initialising travel time data and happiness for all commuters...";
		ask Commuters {
			evalAddress <- home;
			do buildRequests;
		}

		// Save the remainder of data.
		if nLines > 0 {
			save toSave to: "../fileWatcher/observed/tempRequests.txt" format: text header: false rewrite: false;
			toSave <- "";
		}

		do sendInitRequest;
		ask Commuters {
			do updateHappiness;
		}

	}

	reflex endSim {
		if cycle > lastCycle {
			do pause;
		}

	}

	reflex relocateUnhappyCommuters {
		nToRelocate <- length(Commuters where (each.patience <= 0));
		listVacantHomes <- Addresses where (each.vacancies > 0);
		ask listVacantHomes {
			do transferVacad;
		}

		write "Relocating " + string(nToRelocate) + " agents, " + string(length(listVacantHomes)) + " vacant addresses...";
		ask Commuters where (each.patience <= 0) {
			loop h over: listVacantHomes {
			// Prefilter addresses based on rent and distance.
				if h.aRent <= maxRent and h distance_to cJob <= dToJobThreshold and h.normDensity < normDensityThreshold {
					evalAddress <- h;
					do buildRequests;
				}

			}

		}

		// Save the remainder of data.
		if nLines > 0 {
			save toSave to: tempRequestsPath format: text header: false rewrite: false;
			toSave <- "";
		}

		if file_exists(tempRequestsPath) and file_exists(tempAddressesPath) {
			nToRelocate <- length(Commuters where (each.patience <= 0));
			nRelocated <- 0;
			do sendRelocationRequest;
			write string(nRelocated) + ' commuters relocated, ' + string(nToRelocate - nRelocated) + ' not relocated.';
		}

	}

	reflex changeCommutersJobs {
		write "Changing " + string(jobChangesPerYear / yearToCycle * 100) + "% of commuters' jobs, recalculating travel time...";
		ask Commuters where (not (each.jobless)) {
			do spontaneousJobChange;
		}

		// Save the remainder of data.
		if nLines > 0 {
			save toSave to: "../fileWatcher/observed/tempRequests.txt" format: text header: false rewrite: false;
			toSave <- "";
		}

		do sendInitRequest;
		ask Commuters {
			do updateHappiness;
		}

	}

	reflex increasePopulation when: Jobs sum_of (each.nJobsLeft) > demographicGrowth and Addresses sum_of (each.vacancies) > demographicGrowth {
		create Commuters number: demographicGrowth {
			maxRent <- rnd(22.0, 30.0);
			home <- one_of(Addresses where (each.vacancies > 0 and each.aRent < self.maxRent));
			if home = nil {
				home <- one_of(Addresses where (each.vacancies > 0));
			}

			ask home {
				do addCommuter;
			}

			location <- home.location;
			cJob <- one_of(Jobs where (each.nJobsLeft > 0));
			ask cJob {
				nJobsLeft <- nJobsLeft - 1;
			}

			if flip(proportionPreferringLowDensity) {
				tThreshold <- highTThreshValue;
				normDensityThreshold <- lowDensityValue;
				color <- #pink;
			}

			do getClosestStopsToJob;
			do updateHappiness;
		}

	}

	reflex saveOutput {
		ask Addresses {
			save name + ";" + nCommuters + ";" + vacancies + ";" + newHomesBuilt + ";" + nMovedIn + ";" + nMovedOut + ";" + aRent + ";" + lon + ";" + lat to:
			outputAddressesFolder + "/Addresses" + cycle + ".txt" format: text header: false rewrite: false;
		}

		ask Commuters {
			save
			name + ";" + home + ";" + cJob + ";" + maxRent + ";" + travelTime + ";" + happy + ";" + patience + ";" + initialPatience + ";" + nRelocations + ";" + home.lon + ";" + home.lat
			to: outputCommutersFolder + "/Commuters" + cycle + ".txt" format: text header: false rewrite: false;
		}

	}

	action sendInitRequest {
		write "Sending request.";
		deleteResponsesOK <- delete_file(responsesPath);
		deleteRequestsOK <- delete_file(requestsPath);
		copyOK <- copy_file(tempRequestsPath, requestsPath);
		deleteTempRequestsOK <- delete_file(tempRequestsPath);
		write "Awaiting response...";
		loop while: not (file_exists(responsesPath)) {
		}

		write "Response received.";
		processOK <- false;
		write "Attempting to process response...";
		loop while: not (processOK) {
			try {
				do processResponse;
			}

			catch {
				processOK <- false;
				write "File reading error, restarting response processing...";
			}

		}

		write "Response processed.";
		deleteResponsesOK <- delete_file(responsesPath);
		write "Responses deleted? " + deleteResponsesOK;
	}

	action sendRelocationRequest {
		deleteAddressesOK <- delete_file(addressesPath);
		copyOK <- copy_file(tempAddressesPath, addressesPath);
		deleteTempAddressesOK <- delete_file(tempAddressesPath);
		do sendInitRequest;
	}

	action processResponse {
		if length(matrix(csv_file(responsesPath, ';'))) > 0 {
			write 'Response ready to be read, processing response...';
			loop r over: rows_list(matrix(csv_file(responsesPath, ';'))) {
				c <- Commuters first_with (each.name = r[0]);
				if c != nil {
					ask c {
					// r[3] is now TTthreshold
						if home != nil and r[1] = home.name {
							travelTime <- int(r[2]);
						} else if home != nil and r[1] != '' and r[1] != home.name {
							newHome <- Addresses first_with (each.name = r[1]);
							minTTeval <- int(r[2]);
							do relocate;
						}

					}

					processOK <- true;
				}

			}

		}

	}

}

species RentSectors schedules: [] {
	float rent;
}

species communesBorder schedules: [] {

	aspect base {
		draw shape color: rgb(245, 234, 218) border: #black;
	}

}

species lake schedules: [] {

	aspect base {
		draw shape color: rgb(208, 234, 245) border: #black;
	}

}

species tramLines schedules: [] {
	string lineID;
	rgb color;

	init {
		if lineID = "12" {
			color <- rgb(245, 163, 0);
		} else if lineID = "14" {
			color <- rgb(90, 30, 130);
		} else if lineID = "15" {
			color <- rgb(132, 71, 28);
		} else if lineID = "17" {
			color <- rgb(0, 172, 231);
		} else if lineID = "18" {
			color <- rgb(184, 47, 137);
		} }

	aspect base {
		draw shape color: color;
	} }

species tpgStops schedules: [] {
	float lon;
	float lat;
	float addressWalkt;
	float jobWalkt;

	aspect base {
		draw circle(5) color: #black;
	}

}

species Addresses {
	int volumesum;
	float normDensity;
	int nCommuters <- 0;
	int vacancies <- 0;
	float lon;
	float lat;
	float aRent;
	int newHomesBuilt <- 0;
	int nMovedIn <- 0;
	int nMovedOut <- 0;

	init {
		aRent <- (RentSectors closest_to self).rent;
	}

	action removeCommuter {
		vacancies <- vacancies + 1;
		nMovedOut <- nMovedOut + 1;
		nCommuters <- nCommuters - 1;
	}

	action addCommuter {
		vacancies <- vacancies - 1;
		nMovedIn <- nMovedIn + 1;
		nCommuters <- nCommuters + 1;
	}

	action transferVacad {
		save name + ";" + vacancies to: tempAddressesPath format: text header: false rewrite: false;
	}

	aspect base {
		draw square(15) color: #green;
	}

}

species Jobs schedules: [] {
	int nJobs <- 0;
	int nJobsLeft;
	rgb color <- #black;
	int size;
	float lon;
	float lat;

	init {
		nJobsLeft <- nJobs;
	}

	action colorScale {
		color <- rgb(255 - nJobs / maxJobs * 155, 80 - nJobs / maxJobs * 80, 0);
		size <- 20 + int(nJobs / maxJobs * 80);
	}

	aspect base {
		draw circle(size) color: color;
	}

}

//*******************************************//
//*************** PDC AREAS *****************//
//*******************************************//
species PDC_areas {
	rgb color;
	string type;
	float lon;
	float lat;
	bool potentialNewHomes <- true;
	Addresses PDC_address;

	init {
		if "habitation" in type {
		// camo green if housing densification, light green if housing extension.
			if "Densification" in type {
				color <- rgb(132, 153, 137);
			} else if "Extension" in type {
				color <- rgb(215, 250, 224);
			}

		} else if "village" in type {
		// teal if villages
			color <- rgb(147, 241, 229);
		} else if "activités" in type {
		// pink if activites.
			color <- rgb(250, 215, 244);
			potentialNewHomes <- false;
		} else if "mixte" in type {
		// orange if renouvellement mixte.
			color <- rgb(255, 131, 89);
		} else {
		// light yellow otherwise.
			color <- rgb(255, 254, 222);
			potentialNewHomes <- false;
		}

		if potentialNewHomes {
			create Addresses {
				nCommuters <- 0;
				lon <- myself.lon;
				lat <- myself.lat;
				location <- centroid(myself);
				myself.PDC_address <- self;
			}

		} }

	aspect base {
		draw shape color: color;
	} }

species Government {

	init {
		location <- point(0);
	}

	reflex createHomes when: length(Addresses where (each.vacancies > 0)) < demographicGrowth {
		ask PDC_areas where (each.potentialNewHomes) {
			ask PDC_address {
				vacancies <- vacancies + 10;
				newHomesBuilt <- newHomesBuilt + 10;
			}

		}

		write "10 homes created in all suitable PDC areas.";
	}

	reflex createJobs when: length(Jobs where (each.nJobsLeft > 0)) < demographicGrowth {
		ask one_of(Jobs) {
			nJobs <- nJobs + 1;
			nJobsLeft <- nJobsLeft + 1;
		}

		write "One job created.";
	}

	aspect base {
		draw circle(20) color: #red;
	}

}

species Commuters {
	bool homeless <- false;
	bool jobless <- false;
	Addresses home; // The commuter's own home.
	Addresses newHome;
	int travelTime <- 120;
	Jobs cJob; // The commuter's own job.
	int dToJob;
	int dToJobThreshold <- 50000;
	int tThreshold <- 35;
	bool happy <- true;
	int initialPatience <- (yearToCycle * 5 / 6) + rnd(yearToCycle * 1 / 3);
	int patience <- initialPatience;
	rgb color <- #blue;
	float lonA;
	float latA;
	float lonB;
	float latB;
	int minTTeval;
	Addresses evalAddress;
	int nRelocations <- 0;
	float maxRent;
	list<tpgStops> stopsAddress;
	list<tpgStops> stopsJob;
	float walkdThreshold <- 400.0;
	float walkt;
	float speed <- 4000 / 60;
	float normDensityThreshold <- 1.0;

	aspect base {
		draw square(10) color: color;
	}

	// Flagship reflex of the model. Decision to move out is based on the happiness variable, which can be influenced by many factors:
	// Distance to city center, commute time, public transport availability, network saturation, #shops and leisure in close proximity, aesthetical factors (greenness, parks, ...), socio-economical factors (wage, suburb reputation, etc...).
	// Each agent can react differently to each of these factors, based on their personality traits, social class, age, etc...
	action updateHappiness {
		if (homeless or home = nil) {
			happy <- jobless;
		} else {
			dToJob <- int(home distance_to cJob);
			if happy != (travelTime < tThreshold and home.normDensity < normDensityThreshold) {
			// write 'Happiness of ' + name + ' changed from ' + happy + ' to ' + not (happy);
			}

			happy <- travelTime < tThreshold and home.normDensity < normDensityThreshold;
		}

		if happy {
			color <- #blue;
		} else {
			color <- #red;
		}

	}

	action getClosestStopsToJob {
	// stopsJob <- tpgStops where (each distance_to cJob <= walkdThreshold);
		ask cJob {
			myself.stopsJob <- tpgStops at_distance myself.walkdThreshold;
		}

		ask stopsJob {
			jobWalkt <- (self distance_to myself.cJob) / myself.speed;
		}

	}

	action buildRequests {
	// get list of all close stops, and compute their distance to address.
	// List of all closest stops to job is already available because it is built 1/ at init commuters' job, 2/ whenever a new commuter enters the sim and gets a job, 3/ whenever a commuter changes job.
	// Always use at_distance or closest_to instead of distance_to, it is much faster. Don't use the following:
	// stopsAddress <- tpgStops where (each distance_to evalAddress <= walkdThreshold);
		ask evalAddress {
			myself.stopsAddress <- tpgStops at_distance myself.walkdThreshold;
		}

		ask stopsAddress {
			addressWalkt <- (self distance_to myself.evalAddress) / myself.speed;
		}

		loop sA over: stopsAddress {
			loop sJ over: stopsJob {
			// compute walking duration from each stop to job;
				walkt <- sA.addressWalkt + sJ.jobWalkt;
				toWrite <- name + ";" + evalAddress.name + ";" + sA.lon + ";" + sA.lat + ";" + sJ.lon + ";" + sJ.lat + ";" + walkt + ";" + tThreshold + "\n";
				toSave <- toSave + toWrite;
				nLines <- nLines + 1;
			}

		}

		// Roughly 100 characters per line, 12000 lines per Mo of data, save every X Mo.
		// Don't forget to save the remaining bit of data at the end !
		// For some reason this takes longer....
		//if nLines > 12000 * 0 {
		save toSave to: tempRequestsPath format: text header: false rewrite: false;
		toSave <- "";
		nLines <- 0;
		//}

	}

	action relocate {
		if home != nil and newHome != nil {
			ask home {
				do removeCommuter;
			}

		}

		home <- newHome;
		location <- home.location;
		nRelocations <- nRelocations + 1;
		homeless <- false;
		travelTime <- minTTeval;
		ask home {
			do addCommuter;
		}

		patience <- initialPatience;
		do updateHappiness;
		newHome <- nil;
		minTTeval <- nil;
		// write "New home found for " + name + ", agent relocated.";
		nRelocated <- nRelocated + 1;
	}

	// this reflex should never trigger.
	// Currently missing a build and send requests action.
	reflex findJob when: jobless and Jobs sum_of (each.nJobsLeft) > 0 {
		cJob <- one_of(Jobs where (each.nJobsLeft > 0));
		if cJob != nil {
			ask cJob {
				nJobsLeft <- nJobsLeft - 1;
			}

			jobless <- false;
			do getClosestStopsToJob;
		}

		do updateHappiness;
	}

	reflex decreasePatience when: not (happy) {
		patience <- patience - 1;
	}

	// Each year, 7.1% of Geneva inhabitants move out. 3.1% in the same commune, 4% in another commune.
	// To be adapted to match the 7% per year quota once time management is implemented.
	// This figure is the target for validation.

	// Each year, 12.7% of job changes (data from 2018)
	action spontaneousJobChange {
		if flip(jobChangesPerYear / yearToCycle) and not (jobless) and Jobs sum_of (each.nJobsLeft) > 0 {
			ask cJob {
				nJobsLeft <- nJobsLeft + 1;
			}

			cJob <- one_of(Jobs where (each.nJobsLeft > 0) - cJob);
			ask cJob {
				nJobsLeft <- nJobsLeft - 1;
			}

			do getClosestStopsToJob;

			// to add: verify if new travel time is still below threshold.
			evalAddress <- home;
			do buildRequests;
		}

	}

}

experiment DPSIR_v2_wholeCanton type: gui {
	output {
		display Environment type: java2D {
			species communesBorder aspect: base;
			species PDC_areas aspect: base;
			species lake aspect: base;
			species tramLines aspect: base;
			//species tpgStops aspect: base;
			species Government aspect: base;
			species Jobs aspect: base;
			species Addresses aspect: base;
			species Commuters aspect: base;
		}

		monitor "Date" value: current_date refresh: every(1 #cycle);
		monitor "Happy commuters [%]" value: (100 * length(Commuters where (each.happy)) / length(Commuters)) with_precision 1 refresh: every(1 #cycle);
		monitor "Average distance to job" value: Commuters mean_of (each.dToJob) refresh: every(1 #cycle);
		display "Happiness" type: java2D {
			chart "Happy commuters" type: series {
				data "Happy commuters [%]" value: (100 * length(Commuters where (each.happy)) / length(Commuters)) color: #green;
				data "Unhappy commuters [%]" value: (100 * length(Commuters where (not (each.happy))) / length(Commuters)) color: #red;
			}

		}

		display "Distance to work" type: java2D {
			chart "Average distance" type: series {
				data "Average distance to work [m]" value: Commuters mean_of (each.dToJob) color: #blue;
			}

		}

	}

}

experiment Batch_DPSIR_WIP type: batch repeat: 5 until: (cycle = 20) {

	reflex end_of_runs {
		int i_run <- 0;
		ask simulations {
			save Addresses format: "shp" to: "../output/DPSIR-model/Addresses" + i_run + ".shp" attributes: ["newHomesBuilt"::newHomesBuilt];
			i_run <- i_run + 1;
		}

	}

}
