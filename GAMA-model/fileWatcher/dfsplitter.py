import pandas as pd
import numpy as np

wholedf = pd.read_csv('observed/dummy-TPG-travel-DB.csv')

splitdf = np.array_split(wholedf, 3)

i= 0
for chunk in splitdf:
    i+=1
    chunk.to_csv(f'observed/splitdf/chunk{i}.csv', index=False)
