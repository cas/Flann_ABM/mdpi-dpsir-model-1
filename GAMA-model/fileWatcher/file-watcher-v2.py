import time
import os
import pandas as pd

def treatRequest(obsFile, refDF):
    request = pd.read_csv(obsFile, sep=';', header=None)
    request.columns = ['name', 'address', 'lonA', 'latA', 'lonB', 'latB', 'walkt']

    # This merge returns intersection of the two df based on the "on" fields.
    window = pd.merge(refDF, request, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
    window['totalduration'] = window.duration + window.walkt

    response = window[['name', 'address', 'totalduration']]
    print(response)
    # For the same given commuter and address, select only the row with minimum total duration.
    response = response.loc[response.groupby(['name', 'address']).totalduration.idxmin()].reset_index(drop=True)
    print(response)
    # GAMA loops work better with .txt formats.
    response.to_csv('observed/response.txt', index=False, header=False, sep=';')
    request.to_csv('observed/debugrequest.csv', index=False, header=False)
    response.to_csv('observed/debugresponse.csv', index=False, header=False)


def fileWatcher(obsFile, refDF, maxRuntime = 60, pollTime = 1):
    i = 0
    while i < maxRuntime * 60 / pollTime:
        if os.path.isfile(obsFile):
            print("Incoming request detected, generating reponse...")
            treatRequest(obsFile, refDF)
            os.remove(obsFile)
            print("Travel time information sent, request deleted.")

        # If requests file is empty / does not exist - do nothing.    
        else:
            print("No incoming request, sleeping for " + str(pollTime) + " seconds (total: " + str(i*pollTime) + "/" + str(maxRuntime * 60) + " seconds).")
        
        i += 1      
        time.sleep(pollTime)


ref = pd.read_csv('observed/full-replaced-errors-final-TPGDB.csv', sep = ';')
ref = ref.drop(columns=['StopA', 'StopB', 'lTT'])
ref = ref.rename(columns={'minTT':'duration'})
print(ref)
requestsFile = 'observed/requests.txt'
fileWatcher(requestsFile, ref, 300)
