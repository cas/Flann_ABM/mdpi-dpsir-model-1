import pandas as pd

incomplete = pd.read_csv(f'observed/splitdf/full1.csv', sep=';')
print(incomplete.dtypes)

errors = incomplete[incomplete.minTT.str.contains('Error', na=False)]
print(errors)

# Mind the negation ~
# Keep ErrorNoTrips
noErrors = incomplete[~incomplete.minTT.str.contains('Error5', na=False)]
noErrors.to_csv(f'observed/splitdf/noerrors_incomplete.csv', index=False, sep=';')


indf = pd.read_csv('observed/dummy-TPG-travel-DB.csv')
print(indf)

# Merge original df with a large subset of that df.
# indicator true labels if the line has been found in only the left df, the right one, or both.
# Filter by only left df.
# Drop somehow drops all lines from resulting filter, from the original merge.
missing = indf.merge(
    noErrors, how='outer', indicator=True
).query('_merge == "left_only"').drop('_merge', axis=1)



missing = missing.drop(columns=['minTT', 'lTT'])
print(missing)

missing.to_csv(f'observed/splitdf/chunk4.csv', index=False, sep=',')