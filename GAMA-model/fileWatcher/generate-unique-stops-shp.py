import pandas as pd
import geopandas as gpd
import geopy.distance
from time import *
import random

# 4 km/h converted to m/min
walking_speed = 4000 / 60  

gdf = gpd.read_file('observed/montees-par-arret-par-ligne.shp')
gunique = gdf.drop_duplicates(subset=['arret'], ignore_index=True)
# print(gunique)
# gunique.to_file('observed/tpgStops.shp')

gdf2 = gpd.read_file('observed/tpgStops.shp')
# print(gdf2.tail(10))

df = pd.read_csv('observed/montees-par-arret-par-ligne.csv', sep = ';')
unique = df.drop_duplicates(subset=['Stop'], ignore_index=True)
# print(unique.tail(10))

gdf2 = gdf2.rename(columns={'arret':'Stop'})
gdf2.Stop = unique.Stop
print(gdf2.tail(10))

gdf2.to_file('observed/tpgStops.shp')