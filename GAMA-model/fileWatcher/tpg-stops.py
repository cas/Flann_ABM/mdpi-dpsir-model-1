import pandas as pd
import geopandas as gpd
import geopy.distance
from time import *

# 4 km/h converted to m/min
walking_speed = 4000 / 60  

df = pd.read_csv('observed/dummy-TPG-travel-DB.csv')

print(df.head(10))

# # This merge returns intersection of the two df based on the "on" fields.
# new = pd.merge(unique, first10, how='inner', on=['Stop'])

# print(new)
