import pandas as pd

# chunk1 = pd.read_csv(f'observed/splitdf/outchunk1.csv', sep=';')
# chunk2 = pd.read_csv(f'observed/splitdf/outchunk2.csv', sep=';')
# chunk3 = pd.read_csv(f'observed/splitdf/outchunk3.csv', sep=';')

# df = pd.concat([chunk1, chunk2, chunk3], ignore_index=True)
# print(df)

# df.to_csv(f'observed/splitdf/full1.csv', index=False, sep=';')


noerrors_incomplete = pd.read_csv(f'observed/splitdf/noerrors_incomplete.csv', sep=';')
chunk4 = pd.read_csv(f'observed/splitdf/outchunk4.csv', sep=';')

df = pd.concat([noerrors_incomplete, chunk4], ignore_index=True)
print(df)

df.to_csv(f'observed/splitdf/full1.csv', index=False, sep=';')