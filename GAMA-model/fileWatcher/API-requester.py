import time as vanilla_time
from datetime import * 
import os

import requests
from concurrent.futures import ThreadPoolExecutor
import pytz
import math

import pandas as pd
import ast


def treatRequest(l):
    global nErrors

    incomingResponse = sendRequest(xmlGenerator(l))
    responseStatus = checkResponse(incomingResponse)
    
    if responseStatus != "ok":
        nErrors += 1 
        l.append(f'Error{responseStatus}')
        l.append(f'Error{responseStatus}')         
        return l 
    else:
        minTravelTimeResult, listResults = travelTime(incomingResponse)
        if type(minTravelTimeResult) == int:
            l.append(minTravelTimeResult)
            l.append(listResults)         
            return l
        else:
            print(minTravelTimeResult)
            nErrors += 1
            l.append('ParseError')
            l.append('ParseError')         
            return l 
            

def xmlGenerator(l):
    """
    Input parameters : list latlon coordinates of departure & arrival places + time of request.

    LatLon info has to be generated in ArcGIS Pro into the attribute table of the layer.
    Analysis -> Tools -> Add Geometry Attributes.
    Centroid coordinates, Coordinate System = GCS_WGS_1984.

    Place all variables in the xml template to create custom XML payload.
    """

    lonA, latA, lonB, latB = l

    # Request time.
    ReqTime = datetime.now(utc).strftime('%Y-%m-%dT%H:%M:%S.999Z')
    # Departure time, fixed at 8:15 am on Oct 11, 2023 = wednesday, rush hour, normal schedule (not vacances).
    time = '2023-10-11T08:15:00'

    #print(xml_data_template.format(RequestTime = ReqTime, DepLon = lonA, DepLat = latA, DepTime = time, ArrLon = lonB, ArrLat = latB))

    return xml_data_template.format(RequestTime = ReqTime, DepLon = lonA, DepLat = latA, DepTime = time, ArrLon = lonB, ArrLat = latB)

# This currently has a runtime of 
def sendRequest(xml_data: str):
    """ Send the POST request with the XML payload """
    return requests.post(url, data=xml_data, headers=headers)

def checkResponse(response):
    """ Check the response status code """
    if response.status_code == 200:
        if len(response.text) > 10:
            if len(response.text) > 1000:
                # Print the response text
                # print(response.text)
                print("Requested data received successfully! Response text length is " + str(len(response.text)) + ".")
                return "ok"
            else:
                print("Error: No journey found. Response text length is " + str(len(response.text)) + ".")
                return "NoTrip"
        else:
            print("No data received.")
            return "NoData"
    else:
        print("Error: ", response.status_code)
        return response.status_code


def travelTime(response):
    """
    Search for all journey travel times.
    They are in the following format: <ojp:Duration>PT1H34M</ojp:Duration>
    All individual legs are also in this format. To differentiate:
    Only journey total travel durations are followed by the <ojp:StartTime> tag.
    """

    outData = response.text
    searchString = "<ojp:Duration>"
    minTime = 10000
    listResults = []

    startIdx = 0
    while True:
        # Find the next occurence of our search string inside response text.
        startIdx = outData.find(searchString, startIdx)
        # When arriving at the end of the response text, break the loop.
        if startIdx == -1:
            break
        
        # Include 60 chars after the search tag.
        result = outData[startIdx:startIdx+60]
        
        # If this tag is in the searh result, then we have correctly detected a total travel duration.
        if "<ojp:StartTime>" in result:
            # remove search tag from the result, delete everything after the first <, and remove "PT" from "PT1H34M".
            result = result.replace(searchString, "").split("<")[0][2:]
            # Convert 1H34M into 94M.
            result = timeConverter(result)
            listResults.append(result)
            # If we found a minimal travel duration, record it.
            if result < minTime:
                minTime = result
        # Finally, go to next occurence.
        startIdx += len(searchString)

    if minTime == 10000:
        return "Error: No journey found."
    else:
        return minTime, listResults

def timeConverter (inStr: str):
    """ Convert xxHxxM into a number of minutes. """
    HHMM = inStr.split("H")
    if "H" in inStr:
        nMin = HHMM[1].split("M")[0]
        if nMin == "":
            nMin = 0
        return int(HHMM[0])*60+int(nMin)
    else:
        return int(inStr.split("M")[0])

def totalSecsToTime(totalsecs):
    totalhours = int(totalsecs // 3600)
    totalminutes = int(totalsecs // 60 - 60*totalhours)
    totalseconds = int(totalsecs % 60)
    return totalhours, totalminutes, totalseconds

# Set the API endpoint URL. Test server, not live server.
url = 'https://odpch-api.clients.liip.ch/ojp-passiv-int'

# Set the headers and content type. This is the unlimited key for the test server.
headers = {
    "Content-Type": "application/xml",
    "Authorization": "eyJvcmciOiI2M2Q4ODhiMDNmZmRmODAwMDEzMDIwODkiLCJpZCI6IjNiMzhmNzdkMmZhMzRiMmQ4ZjgzYzk1ZTVmOTc0Zjk2IiwiaCI6Im11cm11cjEyOCJ9"
}

# Define the XML payload to send
xml_data_template = '<?xml version="1.0" encoding="utf-8"?><OJP xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.siri.org.uk/siri" version="1.0" xmlns:ojp="http://www.vdv.de/ojp" xsi:schemaLocation="http://www.siri.org.uk/siri ../ojp-xsd-v1.0/OJP.xsd"><OJPRequest><ServiceRequest><RequestTimestamp>{RequestTime}</RequestTimestamp><RequestorRef>API-Explorer</RequestorRef><ojp:OJPTripRequest><RequestTimestamp>{RequestTime}</RequestTimestamp><ojp:Origin><ojp:PlaceRef><ojp:GeoPosition><Longitude>{DepLon}</Longitude><Latitude>{DepLat}</Latitude></ojp:GeoPosition><ojp:LocationName><ojp:Text>DeparturePoint</ojp:Text></ojp:LocationName></ojp:PlaceRef><ojp:DepArrTime>{DepTime}</ojp:DepArrTime></ojp:Origin><ojp:Destination><ojp:PlaceRef><ojp:GeoPosition><Longitude>{ArrLon}</Longitude><Latitude>{ArrLat}</Latitude></ojp:GeoPosition><ojp:LocationName><obj:Text>ArrivalPoint</obj:Text></ojp:LocationName></ojp:PlaceRef></ojp:Destination><ojp:Params><ojp:IncludeTrackSections>false</ojp:IncludeTrackSections><ojp:IncludeLegProjection>false</ojp:IncludeLegProjection><ojp:IncludeTurnDescription>false</ojp:IncludeTurnDescription><ojp:IncludeIntermediateStops>false</ojp:IncludeIntermediateStops></ojp:Params></ojp:OJPTripRequest></ServiceRequest></OJPRequest></OJP>'

# Timezones.
utc = pytz.utc
paris_tz = pytz.timezone('Europe/Paris')

nErrors = 0

# Test error handling system.
# rep = treatRequest([5.162252,46.201778,6.157364,46.198624])
# print(rep)


# Change this value to 1, 2 or 3 depending on which chunk is used.
nChunk = 4


# Start from where program left off
# Import existing outchunk
# chunk = chunk.tail(len(chunk) - len(import))
# Don't forget to save to another location !!


chunk = pd.read_csv(f'observed/splitdf/chunk{nChunk}.csv')
# chunk = chunk.head(40)
chunk = chunk.drop('duration', axis=1)
lite = chunk[['lonA', 'latA', 'lonB', 'latB']]
lendf = len(lite)

reqHistory = []
listRequests = []
listResponses = []

maxReqPerSec = 10
batchSize = 40
batchPeriod = batchSize / maxReqPerSec

nWorkers = batchSize

startTime = datetime.now()

for i, row in lite.iterrows():

    if len(reqHistory) >= batchSize:
        diff = math.floor(100*(datetime.now() - reqHistory[0]).total_seconds())/100
        if diff <= batchPeriod and diff != 0:
            print(f"Sleeping for {batchPeriod - diff} seconds.")
            vanilla_time.sleep(batchPeriod - diff)
        reqHistory.pop(0)

    reqHistory.append(datetime.now())
    listRequests.append( list(row[:4]) )
    
    if (i+1) % nWorkers == 0:
        b = datetime.now()
        with ThreadPoolExecutor(max_workers = nWorkers) as pool:
            listResponses.extend( list(pool.map(treatRequest, listRequests)) )
            
        print("Total execution time for this batch of " + str(len(listRequests)) + " workers: " + str(round((datetime.now() - b).total_seconds(),2)) + " seconds, " + str(i+1) + ' requests treated, ' + str(lendf-i-1) + ' remaining.')
        elapsedSecsTotal = round((datetime.now() - startTime).total_seconds(),2)
        elapsedHours, elapsedMins, elapsedSecs = totalSecsToTime(elapsedSecsTotal)
        completionPercentage = 100*(i+1)/(lendf)
        print(f'Completion percentage: {round(completionPercentage,3)}%, total time elapsed: {elapsedHours} hours, {elapsedMins} minutes, {elapsedSecs} seconds.')
        remainingSecsTotal = elapsedSecsTotal * (100-completionPercentage) / completionPercentage
        remainingHours, remainingMins, remainingSecs = totalSecsToTime(remainingSecsTotal)
        print(f'Estimated time remaining: {remainingHours} hours, {remainingMins} minutes, {remainingSecs} seconds, {nErrors} errors detected.')
        completionTime = datetime.now() + timedelta(seconds = remainingSecsTotal)
        tprint = completionTime.strftime('%H:%M:%S')
        print(f'Estimated completion time: {tprint}.')
        listRequests = []

    if (i+1) % 400 == 0:
        responses = pd.DataFrame(listResponses)
        responses.columns = ['lonA', 'latA', 'lonB', 'latB', 'minTT', 'lTT']

        outdf = pd.merge(chunk, responses, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
        outdf.to_csv(f'observed/splitdf/outchunk{nChunk}.csv', index=False, sep=';')
        print('Dataframe checkpoint.')  

if listRequests:    
    b = datetime.now()
    with ThreadPoolExecutor(max_workers = nWorkers) as pool:
        listResponses.extend( list(pool.map(treatRequest, listRequests)) )
        
    print("Total execution time for this batch of " + str(len(listRequests)) + " workers: " + str(round((datetime.now() - b).total_seconds(),2)) + " seconds.")
    listRequests = []  

responses = pd.DataFrame(listResponses)
responses.columns = ['lonA', 'latA', 'lonB', 'latB', 'minTT', 'lTT']

outdf = pd.merge(chunk, responses, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
outdf.to_csv(f'observed/splitdf/outchunk{nChunk}.csv', index=False, sep=';')
print('Dataframe saved.')

checkdf = pd.read_csv(f'observed/splitdf/outchunk{nChunk}.csv', sep=';')
print(checkdf)

if nErrors:
    print(f'WARNING! {nErrors} errors found!')
else:
    print('Task completed successfully with no errors.')
