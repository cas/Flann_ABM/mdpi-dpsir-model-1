import pandas as pd
import geopandas as gpd
import geopy.distance
from time import *
import random

# 4 km/h converted to m/min
walking_speed = 4000 / 60  

df = pd.read_csv('observed/montees-par-arret-par-ligne.csv', sep = ';')
print(df.head(10))
df2 = df[['Line', 'Stop', 'Long Code Stop','Longitude','Latitude']]

# Drop duplicates and Line column, will be added later.
unique = df2.drop_duplicates(subset=['Stop'], ignore_index=True)
unique = unique.drop('Line', axis=1)

# For each stop, get a list of all lines that service it.
setoflines = df2.groupby(['Stop'])['Line'].apply(set).apply(list)

# Add this info to the dataset of unique stops.
unique = pd.merge(unique, setoflines, how='inner', on=['Stop'])
unique.to_csv('observed/uniqueStopsLines.csv', index=False, sep=';')

# Drop LongStopCode and Line
unique = unique.drop('Line', axis=1)
unique = unique.drop('Long Code Stop', axis=1)
print(unique)

# Init travel df.
tpgtraveldf = pd.DataFrame(columns = ['StopA', 'lonA', 'latA', 'StopB', 'lonB', 'latB', 'duration'])

n=0

for i1, row1 in unique.iterrows():
    dfi = pd.DataFrame(columns = ['StopA', 'lonA', 'latA', 'StopB', 'lonB', 'latB', 'duration'])
    for i2, row2 in unique.iterrows():
        
        if i1 != i2:
            l = list(row1) + list(row2)
            l.append(10 + random.randint(0,20))
            
            
            n+=1
            if n%100 == 0:
                print(n)

            if n%10000 == 0:
                print(tpgtraveldf.tail(5))    

            # Append line to df
            dfi.loc[(len(dfi)),:] = l
    
    tpgtraveldf = pd.concat([tpgtraveldf, dfi], ignore_index=True)

df_debug = tpgtraveldf.sample(n = 1000, replace = False)
df_debug.to_csv('observed/lite-dummy-TPG-travel-DB.csv', index=False)

tpgtraveldf.to_csv('observed/dummy-TPG-travel-DB.csv', index=False)
print(tpgtraveldf.head(20))
print(tpgtraveldf.tail(20))

# unique['n'] = pd.Series(range(10, len(unique.Stop) + 10))
# print(unique)

# # This merge returns intersection of the two df based on the "on" fields.
# new = pd.merge(unique, first10, how='inner', on=['Stop'])

# print(new)
