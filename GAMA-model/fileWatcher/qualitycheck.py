import pandas as pd

def replaceErrorsMinTT(value):
    if value == 'ErrorNoTrip':
        return 300
    else:
        return value
    
def replaceErrorslTT(value):
    if value == 'ErrorNoTrip':
        return [300]
    else:
        return value


df = pd.read_csv('observed/final-TPG-travel-DB.csv', sep=';')
print(df)

checkdupes = df.drop_duplicates(subset= ['lonA', 'latA', 'lonB', 'latB'])
print('No duplicates found: ' + str(len(checkdupes) == len(df)))


randomcheck = df.sample(20)
print(randomcheck)

errors = df[df.minTT.str.contains('Error', na=False)]

noErrors = df[~df.minTT.str.contains('Error', na=False)]
noErrors.minTT = noErrors.minTT.map(lambda x: int(x))
print(noErrors.dtypes)
noErrors = noErrors.sort_values(by='minTT')

noErrors.to_csv(f'observed/full-no-errors-final-TPGDB.csv', index=False, sep=';')

withErrors = pd.concat([noErrors, errors], ignore_index=True)
print(withErrors)
withErrors.to_csv(f'observed/full-with-errors-final-TPGDB.csv', index=False, sep=';')


replacedErrors = withErrors.copy()
replacedErrors.minTT = replacedErrors.minTT.map(lambda x: replaceErrorsMinTT(x))
replacedErrors.lTT = replacedErrors.lTT.map(lambda x: replaceErrorslTT(x))
print(replacedErrors)
replacedErrors.to_csv(f'observed/full-replaced-errors-final-TPGDB.csv', index=False, sep=';')