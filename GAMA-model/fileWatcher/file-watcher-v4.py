from time import *
import os
import pandas as pd
import geopy.distance
import geopandas as gpd
from shapely.geometry import Point


def treatRequest(obsFile, refDF):
    request = pd.read_csv(obsFile, sep=';', header=None)
    request.columns = ['name', 'address', 'lonA', 'latA', 'lonB', 'latB', 'walkt']

    # This merge returns intersection of the two df based on the "on" fields.
    window = pd.merge(refDF, request, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
    window['totalduration'] = window.duration + window.walkt

    response = window[['name', 'address', 'totalduration']]
    print(response)
    # For the same given commuter and address, select only the row with minimum total duration.
    response = response.loc[response.groupby(['name', 'address']).totalduration.idxmin()].reset_index(drop=True)
    print(response)
    # GAMA loops work better with .txt formats.
    response.to_csv('observed/response.txt', index=False, header=False, sep=';')
    request.to_csv('observed/debugrequest.csv', index=False, header=False)
    response.to_csv('observed/debugresponse.csv', index=False, header=False)


def fileWatcher(obsFile, refDF, maxRuntime = 60, pollTime = 1):
    i = 0
    while i < maxRuntime * 60 / pollTime:
        if os.path.isfile(obsFile):
            print("Incoming request detected, generating reponse...")
            treatRequest(obsFile, refDF)
            os.remove(obsFile)
            print("Travel time information sent, request deleted.")

        # If requests file is empty / does not exist - do nothing.    
        else:
            print("No incoming request, sleeping for " + str(pollTime) + " seconds (total: " + str(i*pollTime) + "/" + str(maxRuntime * 60) + " seconds).")
        
        i += 1      
        time.sleep(pollTime)

def getStopsWithinDistanceOfAddress(address: gpd.GeoSeries, stops_proj_DF, dist=400):
    address_proj = address.to_crs("EPSG:2056")

    buffered = address_proj.buffer(dist).unary_union

    neigh = stops_proj_DF.geometry.intersection(buffered)
    return stops_proj_DF[~neigh.is_empty]

ref = pd.read_csv('observed/full-replaced-errors-final-TPGDB.csv', sep = ';')
ref = ref.drop(columns=['StopA', 'StopB', 'lTT'])
ref = ref.rename(columns={'minTT':'duration'})

#fileWatcher(requestsFile, ref, 600)

# Store stops database, give them a geometry, reproject to swiss CRS for measuring distance in meters.
stops = pd.read_csv('observed/uniqueStopsLines.csv', sep = ';')
stops = gpd.GeoDataFrame(stops, geometry=gpd.points_from_xy(stops.Longitude, stops.Latitude), crs="EPSG:4326")
stops = stops.drop(columns=['Long Code Stop', 'Line'])
stops_proj=stops.to_crs("EPSG:2056")
print(stops_proj)

# 1) read vacant addresses
vacad = pd.read_csv('observed/addressesSamplev4.txt', sep=';', header=None)
vacad.columns = ['aname', 'lon', 'lat', 'rent']
# 2) read commuters to relocate
commuters = pd.read_csv('observed/requestsSamplev4.txt', sep=';', header=None)
commuters.columns = ['cname', 'maxRent', 'joblon', 'joblat']
commuters = gpd.GeoDataFrame(commuters, geometry=gpd.points_from_xy(commuters.joblon, commuters.joblat), crs="EPSG:4326")
commuters = commuters.head(5)



allResponses = pd.DataFrame(columns= ['commuter', 'address', 'duration'])

# Try using apply ?
for commuter in commuters.itertuples(index=False):

    a = time()
    # Job's geometry is stored in the commuter df. get closest stops to job.
    job=gpd.GeoSeries(commuter.geometry, crs="EPSG:4326")
    stopsJob = getStopsWithinDistanceOfAddress(job, stops_proj)
    stopsJob = stopsJob.drop(columns=['geometry']).rename(columns={'Stop':'stopB', 'Longitude':'lonB', 'Latitude': 'latB'})
    stopsJob['commuter'] = commuter.cname

    print(time() - a)

    # 3) for each commuter to relocate, filter addresses that have rent < max rent allowed by commuter.
    adDF = vacad[vacad.rent < commuter.maxRent].reset_index(drop=True)
    adDF['commuter'] = commuter.cname
    adGDF = gpd.GeoDataFrame(adDF, geometry=gpd.points_from_xy(adDF.lon, adDF.lat), crs="EPSG:4326")

    # Start building the dataframe of closest stops to all addresses allowed.
    stopsAds = pd.DataFrame(columns= ['stopA', 'lonA', 'latA', 'address'])

    print(time() - a)

    for ad in adGDF.itertuples(index=False):
        
        adGS = gpd.GeoSeries(ad.geometry,crs="EPSG:4326")

        # Currently, this function takes 40ms per run, which is way too long when used with itertuples.
        stopsAd = getStopsWithinDistanceOfAddress(adGS, stops_proj)

        stopsAd = stopsAd.drop(columns=['geometry']).rename(columns={'Stop':'stopA', 'Longitude':'lonA', 'Latitude': 'latA'})
        stopsAd['address'] = ad.aname
        stopsAds = pd.concat([stopsAds, stopsAd], ignore_index=True)

    print(time() - a)

    # Cartesian product of adresses-stops with job-stops gives us all possible combinations.
    request = pd.merge(stopsAds, stopsJob, how='cross')
    # Fetch travel duration from reference dataframe.
    window = pd.merge(ref, request, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
    
    response = window[['commuter', 'address', 'duration']]
    
    print(time() - a)

    # For the same given commuter and address, select only the row with minimum total duration.
    response = response.loc[response.groupby(['commuter', 'address']).duration.idxmin()].reset_index(drop=True)
    
    allResponses = pd.concat([allResponses, response], ignore_index=True)

print(allResponses)
print(time() - a)

















## LEGACY CODE ##


# ref['pointA'] = tuple(zip(ref.lonA, ref.latA))
# ref['pointB'] = tuple(zip(ref.lonB, ref.latB))
# ref = ref.head(200)
# a = time.time()
# ref['distance'] = ref.apply(lambda x: geopy.distance.geodesic(x.pointA, x.pointB).m, axis=1)
# b = time.time() - a
# print(f'Time spent to calculate distances: {b}.')

# print(ref)



# requestA = gpd.GeoDataFrame(request, geometry=gpd.points_from_xy(request.lonA, request.latA), crs="EPSG:4326")
# addressA = requestA.iloc[0]
# requestB = gpd.GeoDataFrame(request, geometry=gpd.points_from_xy(request.lonB, request.latB), crs="EPSG:4326")
# addressB = requestB.iloc[0]

# print(addressA.geometry)
# addressA = gpd.GeoSeries(addressA.geometry,crs="EPSG:4326")
# stopsFromA = getStopsWithinDistanceOfAddress(addressA, stops_proj)
# stopsFromA = stopsFromA.drop(columns=['geometry']).rename(columns={'Stop':'stopA', 'Longitude':'lonA', 'Latitude': 'latA'})

# addressB = gpd.GeoSeries(addressB.geometry,crs="EPSG:4326")
# stopsFromB = getStopsWithinDistanceOfAddress(addressB, stops_proj)
# stopsFromB = stopsFromB.drop(columns=['geometry']).rename(columns={'Stop':'stopB', 'Longitude':'lonB', 'Latitude': 'latB'})

# request = pd.merge(stopsFromA, stopsFromB, how= 'cross')
# print(request)
# window = pd.merge(ref, request, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
# print(window)
