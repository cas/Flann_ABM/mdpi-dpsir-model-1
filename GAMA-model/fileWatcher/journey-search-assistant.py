import pandas as pd
import geopandas as gpd
import geopy.distance
from time import *

# 4 km/h converted to m/min
walking_speed = 4000 / 60  

df = pd.read_csv('observed/travelDatabase.txt', sep = ';', header = None)
df.columns = ['lonA', 'latA', 'lonB', 'latB', 'duration']

# class Journey():
#     def __init__(self, lonA, latA, lonB, latB):
#         self.lonA = lonA
#         self.latA = latA
#         self.lonB = lonB
#         self.latB = latB




df['pointA'] = df.apply(lambda row: (row.lonA, row.latA), axis = 1)
df['pointB'] = df.apply(lambda row: (row.lonB, row.latB), axis = 1)
df = df.drop(columns=['lonA', 'latA', 'lonB', 'latB'])




testA = (6.11805672358, 46.2291103253)
testB = (6.14022243694, 46.211362094)
print(geopy.distance.geodesic(testA, testB).m)

t1 = time()
df['distance'] = df.apply(lambda row: geopy.distance.geodesic(row.pointA, testA).m
                          + geopy.distance.geodesic(row.pointB, testB).m, axis = 1)

print(df)
df['total_duration'] = df.apply(lambda row: row.duration + row.distance / walking_speed, axis=1)
print("1:", time() - t1)




total_duration = df.total_duration.min()
print(total_duration)

