from time import time, sleep
import os

import requests
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor
import pytz

# Define file locations.
workspace = os.path.dirname(os.getcwd())
myDir = workspace + "\\fileWatcher\\observed"
myRequests = myDir + "\\requests.txt"
myReponses = myDir + "\\responses.txt"
myTravelDatabase = myDir + "\\travelDatabase.txt"


def loadDatabase (travelDB):

    """Import locally stored database of travel times.
    InFormat is lonA; latA; lonB; latB; minTravelTime
    OutFormat is a dictionnary with keys (lonA, latA, lonB, latB) and values minTravelTime
    Warning : dict keys can't be lists as they aren't immutable. Here, keys are tuples, which are immutable."""

    loadedTravelDB = dict()
    with open(travelDB, "r") as db:
        for line in db:
            entry = line.split(";")
            entry = [float(x.rstrip()) for x in entry]
            loadedTravelDB[tuple(entry[:4])] = int(entry[4])

    return loadedTravelDB



def unPackLine(l: str):
    """ requests.txt input file contains the following info: agentName; longitude of point A; latitude A; longitude B; latitude B. """
    els = l.rstrip().split(";")
    return els[0], els[1], float(els[2]), float(els[3]), float(els[4]), float(els[5])


def timeConverter (inStr: str):
    """ Convert xxHxxM into a number of minutes. """
    HHMM = inStr.split("H")
    if "H" in inStr:
        nMin = HHMM[1].split("M")[0]
        if nMin == "":
            nMin = 0
        return int(HHMM[0])*60+int(nMin)
    else:
        return int(inStr.split("M")[0])

def xmlGenerator(lonA: float, latA: float, lonB: float, latB: float):
    """
    Input parameters : latlon coordinates of departure & arrival places + time of request.

    LatLon info has to be generated in ArcGIS Pro into the attribute table of the layer.
    Analysis -> Tools -> Add Geometry Attributes.
    Centroid coordinates, Coordinate System = GCS_WGS_1984.

    Place all variables in the xml template to create custom XML payload.
    """

    # Request time.
    ReqTime = datetime.now(paris_tz).strftime('%Y-%m-%dT%H:%M:%S.999Z')
    # Departure time.
    time = datetime.now(paris_tz).strftime('%Y-%m-%dT%H:%M:%S.999Z')

    return xml_data_template.format(RequestTime = ReqTime, DepLon = lonA, DepLat = latA, DepTime = time, ArrLon = lonB, ArrLat = latB)

# This currently has a runtime of 
def sendRequest(xml_data: str):
    """ Send the POST request with the XML payload """
    return requests.post(url, data=xml_data, headers=headers)

def checkResponse(response):
    """ Check the response status code """
    if response.status_code == 200:
        if len(response.text) > 10:
            # Print the response text
            print("Requested data received successfully! Response text length is " + str(len(response.text)) + ".")
            return "ok"
        else:
            print("No data received.")
            return "noData"
    else:
        print("Error: ", response.status_code)
        return "Error:" + str(response.status_code)

def findMinTravelTime(response):
    """
    Search for all journey travel times.
    They are in the following format: <ojp:Duration>PT1H34M</ojp:Duration>
    All individual legs are also in this format. To differentiate:
    Only journey total travel durations are followed by the <ojp:StartTime> tag.
    """

    outData = response.text
    searchString = "<ojp:Duration>"
    minTime = 10000

    startIdx = 0
    while True:
        # Find the next occurence of our search string inside response text.
        startIdx = outData.find(searchString, startIdx)
        # When arriving at the end of the resonse text, break the loop.
        if startIdx == -1:
            break
        
        # Include 60 chars after the search tag.
        result = outData[startIdx:startIdx+60]
        
        # If this tag is in the searh result, then we have correctly detected a total travel duration.
        if "<ojp:StartTime>" in result:
            # remove search tag from the result, delete everything after the first <, and remove "PT" from "PT1H34M".
            result = result.replace(searchString, "").split("<")[0][2:]
            # Convert 1H34M into 94M.
            result = timeConverter(result)
            # If we found a minimal travel duration, record it.
            if result < minTime:
                minTime = result
        # Finally, go to next occurence.
        startIdx += len(searchString)

    if minTime == 10000:
        return "Error: No journey found."
    else:
        return minTime
        
def treatRequest (l, travelDB: dict, travelDB_FP):

    """ For each requested A -> B journey, either find minimum travel time in stored database, or send a request to the API, then retrieve and process response. """

    agentName, addressName, longA, latitA, longB, latitB = unPackLine(l)

    if (longA, latitA, longB, latitB) in travelDB.keys():
        print("Travel found in database, sending recorded travel time.")
        return agentName + ";" + addressName + ";" + str(travelDB[(longA, latitA, longB, latitB)]) + "\n"
    
    else:
       
        incomingResponse = sendRequest(xmlGenerator(longA, latitA, longB, latitB))
        

        responseStatus = checkResponse(incomingResponse)
        
        if responseStatus != "ok":
            return agentName + ";" + addressName + ";" + responseStatus + "\n"
        else:
            minTravelTimeResult = findMinTravelTime(incomingResponse)
            if type(minTravelTimeResult) == int:
                recordEntry(travelDB, travelDB_FP, longA, latitA, longB, latitB, minTravelTimeResult)
                
                return agentName + ";" + addressName + ";" + str(minTravelTimeResult) + "\n"
            else:
                print(minTravelTimeResult)
                return "Error"
                
        
def recordEntry(travelDB, travelDB_FP, lonA, latA, lonB, latB, travelTime):

    """After each request to the API, we store point A and B lonlat as well as travel time."""

    travelDB [(lonA, latA, lonB, latB)] = travelTime
    with open(travelDB_FP, "a") as db:
        db.write(str(lonA) + ";" + str(latA) + ";" + str(lonB) + ";" + str(latB) + ";" + str(travelTime) + "\n")



def fileWatcher(obsFile: str, outputFile: str, travelDB_FP, nWorkers = 50, pollTime = 1,  maxRuntime = 60, maxReqPerMin = 50):

    """File watcher = Main infinite loop that awaits and treats requests on the fly."""

    i = 0
    timerStart = round(time(),2)
    reqN = 0
    while i < maxRuntime * 60 / pollTime:
        
        # Load travel database
        travelDB = loadDatabase(travelDB_FP)
        # If requests file exists, there is a request that needs to be treated.     
        if os.path.isfile(obsFile):
            print("Incoming request detected, generating reponse...")
            # Store request information.
            with open(obsFile, "r") as f1:
                message = ""
                linesCount = 0
                listRequests = []
                for line in f1:
                    linesCount += 1
                    temp_agentName, temp_addressName, temp_longA, temp_latitA, temp_longB, temp_latitB = unPackLine(line)

                    if (temp_longA, temp_latitA, temp_longB, temp_latitB) not in travelDB.keys():
                        reqN += 1
                        if reqN >= maxReqPerMin :
                            timerQuotaCheck = round(time(),2)
                            if timerQuotaCheck - timerStart <= 60:
                                print ("Quota exceeded, sleeping for " + str(61 - (timerQuotaCheck - timerStart)) + " seconds.")
                                sleep(61 - (timerQuotaCheck - timerStart))
                                timerStart = round(time(),2)
                                reqN = 0

                        
                    listRequests.append(line)
                    if linesCount % nWorkers == 0:
                        b = time()
                        with ThreadPoolExecutor(max_workers = nWorkers) as pool:
                            listResponses = list(pool.map(treatRequest, listRequests, [travelDB for i in range(nWorkers)], [travelDB_FP for i in range(nWorkers)]))
                            for r in listResponses:
                                if r != "Error":
                                    message += r
                        
                        print("Total execution time for this batch of " + str(len(listRequests)) + " workers: " + str(round(time() - b,2)) + " seconds.")
                        listRequests = []

                    #response = treatRequest(line, travelDB, travelDB_FP)
                    #if response != "Error":
                        #message += response
                    #print("Request no." + str(linesCount) + " treated successfully.") 

                # Treat last batch.
                b = time()
                with ThreadPoolExecutor(max_workers = nWorkers) as pool:
                    listResponses = list(pool.map(treatRequest, listRequests, [travelDB for i in range(nWorkers)], [travelDB_FP for i in range(nWorkers)]))
                    for r in listResponses:
                        if r != "Error":
                            message += r

                print("Total execution time for this batch of " + str(len(listRequests)) + " workers: " + str(round(time() - b,2)) + " seconds.")
                listRequests = []

                    
                    
            # Generate response, and store it in output file.        
            with open(outputFile, "w") as f2:
                f2.write(message)
            # Delete requests file.
            os.remove(obsFile)
            print("Minimum travel time information sent, request deleted.")    

        # If requests file is empty - do nothing.    
        else:
            print("No incoming request, sleeping for " + str(pollTime) + " seconds (total: " + str(i*pollTime) + "/" + str(maxRuntime * 60) + " seconds).")
        
        i += 1
        # Wait for X seconds.        
        sleep(pollTime)






# Set the API endpoint URL
url = 'https://api.opentransportdata.swiss/ojp2020'

# Set the headers and content type
headers = {
    "Content-Type": "application/xml",
    "Authorization": "eyJvcmciOiI2NDA2NTFhNTIyZmEwNTAwMDEyOWJiZTEiLCJpZCI6IjZiNWMwZDU0MDU3NDRmNWNiYzZjY2E0NTgyZTE2YmU1IiwiaCI6Im11cm11cjEyOCJ9"
}

# Define the XML payload to send
xml_data_template = '<?xml version="1.0" encoding="utf-8"?><OJP xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.siri.org.uk/siri" version="1.0" xmlns:ojp="http://www.vdv.de/ojp" xsi:schemaLocation="http://www.siri.org.uk/siri ../ojp-xsd-v1.0/OJP.xsd"><OJPRequest><ServiceRequest><RequestTimestamp>{RequestTime}</RequestTimestamp><RequestorRef>API-Explorer</RequestorRef><ojp:OJPTripRequest><RequestTimestamp>{RequestTime}</RequestTimestamp><ojp:Origin><ojp:PlaceRef><ojp:GeoPosition><Longitude>{DepLon}</Longitude><Latitude>{DepLat}</Latitude></ojp:GeoPosition><ojp:LocationName><ojp:Text>DeparturePoint</ojp:Text></ojp:LocationName></ojp:PlaceRef><ojp:DepArrTime>{DepTime}</ojp:DepArrTime></ojp:Origin><ojp:Destination><ojp:PlaceRef><ojp:GeoPosition><Longitude>{ArrLon}</Longitude><Latitude>{ArrLat}</Latitude></ojp:GeoPosition><ojp:LocationName><obj:Text>ArrivalPoint</obj:Text></ojp:LocationName></ojp:PlaceRef></ojp:Destination><ojp:Params><ojp:IncludeTrackSections>false</ojp:IncludeTrackSections><ojp:IncludeLegProjection>false</ojp:IncludeLegProjection><ojp:IncludeTurnDescription>false</ojp:IncludeTurnDescription><ojp:IncludeIntermediateStops>false</ojp:IncludeIntermediateStops></ojp:Params></ojp:OJPTripRequest></ServiceRequest></OJPRequest></OJP>'

# Timezones.
utc = pytz.utc
paris_tz = pytz.timezone('Europe/Paris')

# Number of parallel workers.
nParallelWorkers = 50

# Generate a file watcher with given parameters.
fileWatcher(pollTime= 0.05, obsFile = myRequests, outputFile = myReponses, travelDB_FP = myTravelDatabase, nWorkers = nParallelWorkers)
   