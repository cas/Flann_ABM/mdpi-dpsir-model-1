import pandas as pd
import tensorflow as tf

df = pd.read_csv('observed/travelDatabase.txt', sep = ';', header = None)
df.columns = ['lonA', 'latA', 'lonB', 'latB', 'duration']

print(df.head(5))

df.duration = (df.duration / 10).round().astype('int64')
print(df.duration.head(5))

nNeurons_LastLayer = df.duration.max() + 1
print(nNeurons_LastLayer)

df_train = df.sample(frac = 0.8, replace = False)
df_test = df.drop(df_train.index)

y_train = df_train.pop('duration')
x_train = df_train
y_test = df_test.pop('duration')
x_test = df_test

def normalize_tensorize(DF):
    DF = (DF - DF.min()) / (DF.max() - DF.min())
    return tf.convert_to_tensor(DF)

x_train = normalize_tensorize(x_train)
y_train = tf.convert_to_tensor(y_train)
x_test = normalize_tensorize(x_test)
y_test = tf.convert_to_tensor(y_test)


model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(4),
  tf.keras.layers.Dense(16, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(16, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(nNeurons_LastLayer)
])

loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)


model.compile(optimizer='SGD',
              loss=loss_fn,
              metrics=['accuracy'])


model.fit(x_train, y_train, epochs=5)

model.evaluate(x_test,  y_test, verbose=2)



# probability_model = tf.keras.Sequential([
#   model,
#   tf.keras.layers.Softmax()
# ])

