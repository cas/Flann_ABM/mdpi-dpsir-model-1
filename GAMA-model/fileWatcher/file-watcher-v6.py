import time
import os
import pandas as pd

def treatRequest(obsFile, refDF):
    for x in range(100):
        try:
            request = pd.read_csv(obsFile, sep=';', header=None)
            error = False
        except:
            print(f'Attempt {x+1}: File access denied, trying again after 2 seconds')
            error = True
        if error:
            time.sleep(2)
        else:
            break
    request.columns = ['name', 'address', 'lonA', 'latA', 'lonB', 'latB', 'walkt', 'TTthresh']

    # This merge returns intersection of the two df based on the "on" fields.
    window = pd.merge(refDF, request, how='inner', on=['lonA', 'latA', 'lonB', 'latB'])
    window['totalduration'] = window.duration + window.walkt

    response = window[['name', 'address', 'totalduration', 'TTthresh']]
    
    # # For the same given commuter and address, select only the row with minimum total duration.
    response = response.loc[response.groupby(['name', 'address']).totalduration.idxmin()].reset_index(drop=True)
    response.columns = ['Commuter', 'Address', 'TT', 'TTthresh']
    
    # GAMA loops work better with .txt formats.

    request.to_csv('observed/debugrequest.csv', index=False, header=False, sep=';')
    return response

def findNewHome(c, ad, tt, thresh):
    global vacad
    global commuterFoundDF

    foundNewHome = commuterFoundDF.loc[commuterFoundDF.Commuter == c].found.reset_index(drop=True)[0]

    if not foundNewHome:
        vacancies = vacad.loc[vacad.Address == ad].vacancies.reset_index(drop=True)[0]

        if vacancies > 0 and tt < thresh:
            vacad.loc[vacad.Address == ad, ['vacancies']] = vacancies - 1
            commuterFoundDF.loc[commuterFoundDF.Commuter == c, ['found']] = True
            commuterFoundDF.loc[commuterFoundDF.Commuter == c, ['newHome']] = ad
            commuterFoundDF.loc[commuterFoundDF.Commuter == c, ['TT']] = tt


# 1) Get reference database = stop-to-stop travel time data.
ref = pd.read_csv('observed/full-replaced-errors-final-TPGDB.csv', sep = ';')
ref = ref.drop(columns=['StopA', 'StopB', 'lTT'])
ref = ref.rename(columns={'minTT':'duration'})
print(ref)

# 2) File watcher initial values.
maxRuntime = 600
pollTime = 1
obsFile = 'observed/requests.txt'
vacadFile = 'observed/vacad.txt'
refDF = ref

# 3) Start file watcher
i = 0
while i < maxRuntime * 60 / pollTime:
    if os.path.isfile(obsFile) and os.path.isfile(vacadFile):
        print("Incoming RELOCATION request detected, generating reponse...")

        # 1) Treat request.
        response = treatRequest(obsFile, refDF)

        # 2) Generate a database of unique commuters, to fill in with the name of the found address.
        commuterFoundDF = response[['Commuter', 'TTthresh']]
        commuterFoundDF = commuterFoundDF.drop_duplicates(subset=['Commuter'])
        commuterFoundDF['found'] = False
        commuterFoundDF['newHome'] = None
        commuterFoundDF['TT'] = 120
        commuterFoundDF = commuterFoundDF[['Commuter', 'found', 'newHome', 'TT', 'TTthresh']]

        # 3) Register vacant addresses.
        vacad = pd.read_csv(vacadFile, sep=';', header=None)
        vacad.columns = ['Address', 'vacancies']

        # 4) Assign a suitable address to commuter.
        response.apply(lambda x: findNewHome(x.Commuter, x.Address, x.TT, x.TTthresh), axis=1)
        commuterFoundDF = commuterFoundDF.drop(columns=['found'])

        # 5) Export files.
        commuterFoundDF.to_csv('observed/response.csv', index=False, header=False, sep=';')
        commuterFoundDF.to_csv('observed/debugresponse.csv', index=False, header=False, sep=';')

        # 6) Delete input files.
        os.remove(obsFile)
        os.remove(vacadFile)
        print("Travel time information for RELOCATION sent, request deleted.")

    elif os.path.isfile(obsFile) and not os.path.isfile(vacadFile):
        print("Incoming INIT or JOB CHANGE request detected, generating reponse...")

        # 1) Treat request.
        response = treatRequest(obsFile, refDF)

        # 2) Export files.
        response.to_csv('observed/response.csv', index=False, header=False, sep=';')
        response.to_csv('observed/debugresponse.csv', index=False, header=False, sep=';')

        # 3) Delete input files.
        os.remove(obsFile)
        print("Travel time information for INIT or JOB CHANGE sent, request deleted.")


    # If requests file and adresses files are empty / do not exist - do nothing.    
    else:
        print("No incoming request, sleeping for " + str(pollTime) + " seconds (total: " + str(i*pollTime) + "/" + str(maxRuntime * 60) + " seconds).")
    
    i += 1      
    time.sleep(pollTime)



