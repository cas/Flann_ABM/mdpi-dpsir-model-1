# It works !!!

import time
import os
import pandas as pd

df = pd.read_csv('observed/debugResponse.csv', sep = ';', header=None)
df.columns=['Commuter', 'Address', 'TT']
print(df)

def findNewHome(c, ad, tt, thresh=35):
    global addf
    global commuterFoundDF

    foundNewHome = commuterFoundDF.loc[commuterFoundDF.Commuter == c].found.reset_index(drop=True)[0]

    if not foundNewHome:
        vacancies = addf.loc[addf.Address == ad].vacancies.reset_index(drop=True)[0]

        if vacancies > 0 and tt < thresh:
            addf.loc[addf.Address == ad, ['vacancies']] = vacancies - 1
            commuterFoundDF.loc[commuterFoundDF.Commuter == c, ['found']] = True
            commuterFoundDF.loc[commuterFoundDF.Commuter == c, ['newHome']] = ad

addf = pd.DataFrame(df.Address.unique())
print(df.Commuter.nunique())
addf.columns=['Address']
addf['vacancies'] = 1 # To replace with real data

commuterFoundDF = pd.DataFrame(df.Commuter.unique())
commuterFoundDF.columns=['Commuter']
commuterFoundDF['found'] = False
commuterFoundDF['newHome'] = None

df.apply(lambda x: findNewHome(x.Commuter, x.Address, x.TT), axis=1)
print(commuterFoundDF)
print(addf)