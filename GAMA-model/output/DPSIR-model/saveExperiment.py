import os, shutil

savedDatasetsPath = '../../../../OutputDatasets'

# List all directories in the desired folder, only keep those which contain 'DPSIR', find next number, save everything.
if os.path.isdir("newDataAddresses") and os.path.isdir("newDataCommuters") and os.path.isfile('app.py') and os.path.isfile('readme.txt') and os.path.isdir(savedDatasetsPath):
    l = os.listdir(savedDatasetsPath)
    l = [x for x in l if 'DPSIR' in x and os.path.isdir(f'{savedDatasetsPath}/{x}')]
    l = [int(y.split('DPSIR')[1]) for y in l]
    new= max(l)+1
    newpath=f'{savedDatasetsPath}/DPSIR{new}'
    os.mkdir(newpath)
    shutil.copytree(src= 'newDataAddresses', dst= f'{newpath}/Addresses')
    shutil.copytree(src= 'newDataCommuters', dst= f'{newpath}/Commuters')
    shutil.copyfile(src= 'app.py', dst= f'{newpath}/app.py')
    shutil.copyfile(src= 'readme.txt', dst= f'{newpath}/readme.txt')
    print('Dataset exported successfully.')
    
else:
    print('Missing files detected:')
    if not os.path.isdir("newDataAddresses"):
        print('No data found for addresses.')
    if not os.path.isdir("newDataCommuters"):
        print('No data found for commuters.')
    if not os.path.isfile('app.py'):
        print('No app.py found.')
    if not os.path.isfile('readme.txt'):
        print('No readme found.')


