import os, shutil

if os.path.isdir("Addresses"):
    shutil.rmtree("Addresses")

if os.path.isdir("Commuters"):
    shutil.rmtree("Commuters")

shutil.copytree(src= 'newDataAddresses', dst= 'Addresses')
shutil.copytree(src= 'newDataCommuters', dst= 'Commuters')

print('Files successfully saved.')